SET IDENTITY_INSERT Suppliers ON
insert into Suppliers(SupplierID, CompanyName,ContactName, ContactTitle,Address, City,Region,PostalCode, Country,Phone,Fax, HomePage) values
(31,'Jackson&Co','Jackson', 'Some text','KremenCity 53, 100','Boston','MA','02134','USA','(100)556-5546',NULL,NULL),
(32,'Jackson&Co','Jackson', 'Some text','KremenCity 53, 100','Bend','OR','02134','USA','(100)556-5546',NULL,NULL),
(33,'Jackson&Co','Jackson', 'Some text','KremenCity 53, 100','Bend','OR','02134','USA','(100)556-5546',NULL,NULL),
(34,'Jackson&Co','Jackson', 'Some text','KremenCity 53, 100','New Orleans','LA','02134','USA','(100)556-5546',NULL,NULL)
SET IDENTITY_INSERT Suppliers OFF
SELECT Region, City, COUNT(City) as 'Количество' 
FROM Suppliers
WHERE Country='USA'
GROUP BY Region, City
HAVING COUNT(City)>1
GO