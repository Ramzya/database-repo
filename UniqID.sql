USE Paintings;
go
Create Procedure UniqID
@table_name varchar(32),
@uniqID varchar(32)
AS
SET @uniqID=@table_name+cast(newid()as varchar(255));
GO
DECLARE @uniqID varchar(32), @table_name varchar(32)
set @table_name='OrderDetails'
EXEC UniqID OrderDetails, @uniqID OUTPUT
SELECT @uniqID
