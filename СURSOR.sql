USE Paintings;
GO

CREATE TYPE UserTableType AS TABLE
(SomeInfo VARCHAR(32),
SomeSecondInfo VARCHAR(128));
GO

CREATE PROCEDURE InfoInserter
	@TestTable UserTableType READONLY
AS
BEGIN
	DECLARE @Id varchar(50);
	DECLARE @Name VARCHAR(32);
	DECLARE @Desc VARCHAR(128);	
	DECLARE MyCursor CURSOR FORWARD_ONLY 
	FOR SELECT SomeInfo, SomeSecondInfo FROM  @TestTable
	OPEN MyCursor
FETCH NEXT FROM MyCursor INTO @Name,@Desc
WHILE @@FETCH_STATUS = 0
BEGIN

	exec UniqID 'Master_alt', @Id OUTPUT
	INSERT INTO Master_alt([id], MasresName,DateofBirth)
	VALUES (@Id, @Name,@Desc)
	FETCH NEXT FROM MyCursor INTO @Name, @Desc
END
CLOSE MyCursor
END
GO
DECLARE @user_table AS UserTableType;
INSERT INTO @user_table(SomeInfo, SomeSecondInfo)
VALUES('Davinchi','MonaLisa'),
('Mikelangelo','SomeFamous painting'),
('Vangog','Some Another Famous Painting');

EXEC InfoInserter @user_table;
Go

SELECT * FROM Master_alt;