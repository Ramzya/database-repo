USE [Paintings]
GO

/****** Object:  Table [dbo].[Customer]    Script Date: 11.10.2018 05:06:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Customer](
	[Customer name] [text] NOT NULL,
	[Order number] [int] NOT NULL,
	[Country] [text] NOT NULL,
	[City] [text] NOT NULL,
	[Post code] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_Order details] FOREIGN KEY([Order number])
REFERENCES [dbo].[Order details] ([Order number])
GO

ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_Order details]
GO




USE [Paintings]
GO

/****** Object:  Table [dbo].[Exhibition]    Script Date: 11.10.2018 05:06:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Exhibition](
	[Exhibition id] [int] NOT NULL,
	[Name] [text] NOT NULL,
	[Date of begining] [date] NOT NULL,
	[Date of ending] [date] NOT NULL,
	[Tickets sold] [int] NOT NULL,
	[Painting sold] [int] NOT NULL,
 CONSTRAINT [PK_Exhibition] PRIMARY KEY CLUSTERED 
(
	[Exhibition id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO






USE [Paintings]
GO

/****** Object:  Table [dbo].[Master]    Script Date: 11.10.2018 05:07:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Master](
	[Master id] [int] NOT NULL,
	[Master`s name] [text] NOT NULL,
	[Date of birth] [date] NULL,
 CONSTRAINT [PK_Master] PRIMARY KEY CLUSTERED 
(
	[Master id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO





USE [Paintings]
GO

/****** Object:  Table [dbo].[Order details]    Script Date: 11.10.2018 05:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Order details](
	[Order number] [int] NOT NULL,
	[Painting id] [int] NOT NULL,
	[Exhibition id] [int] NOT NULL,
 CONSTRAINT [PK_Order details] PRIMARY KEY CLUSTERED 
(
	[Order number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Order details]  WITH CHECK ADD  CONSTRAINT [FK_Order details_Painting2] FOREIGN KEY([Painting id])
REFERENCES [dbo].[Painting] ([Painting id])
GO

ALTER TABLE [dbo].[Order details] CHECK CONSTRAINT [FK_Order details_Painting2]
GO





USE [Paintings]
GO

/****** Object:  Table [dbo].[Painting]    Script Date: 11.10.2018 05:08:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Painting](
	[Painting id] [int] NOT NULL,
	[Name] [text] NOT NULL,
	[Style] [text] NOT NULL,
	[Cost] [money] NOT NULL,
	[Date of creation] [date] NOT NULL,
	[Master id] [int] NOT NULL,
	[Exhibition id] [int] NOT NULL,
 CONSTRAINT [PK_Painting] PRIMARY KEY CLUSTERED 
(
	[Painting id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Painting]  WITH CHECK ADD  CONSTRAINT [FK_Painting_Exhibition] FOREIGN KEY([Exhibition id])
REFERENCES [dbo].[Exhibition] ([Exhibition id])
GO

ALTER TABLE [dbo].[Painting] CHECK CONSTRAINT [FK_Painting_Exhibition]
GO

ALTER TABLE [dbo].[Painting]  WITH CHECK ADD  CONSTRAINT [FK_Painting_Master] FOREIGN KEY([Master id])
REFERENCES [dbo].[Master] ([Master id])
GO

ALTER TABLE [dbo].[Painting] CHECK CONSTRAINT [FK_Painting_Master]
GO






