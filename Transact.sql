use Paintings;
GO
CREATE PROCEDURE usp_GetErrorInfo  
AS  
SELECT  
    ERROR_NUMBER() AS ErrorNumber  
    ,ERROR_SEVERITY() AS ErrorSeverity  
    ,ERROR_STATE() AS ErrorState  
    ,ERROR_PROCEDURE() AS ErrorProcedure  
    ,ERROR_LINE() AS ErrorLine  
    ,ERROR_MESSAGE() AS ErrorMessage;  
GO  
CREATE PROC TransactXML @Request XML
AS
DECLARE @xmlDoc int;
EXEC sp_XML_preparedocument  @xmlDoc OUTPUT,  @Request
INSERT CustomerSecond
SELECT * FROM OPENXML(@xmlDoc, 'Customer/Customer',2)
WITH Customer
BEGIN TRY
	BEGIN TRANSACTION MyTransact 
	
	
	MERGE CustomerSecond trg
USING Customer src 
ON trg.CustomerName=src.CustomerName AND trg.Country=src.Country


WHEN NOT MATCHED BY SOURCE THEN
  DELETE

-- 2. ���� ������������� ������ trg �� ������� �� ��������� src
WHEN MATCHED THEN
  UPDATE SET
    trg.OrderNumber=src.OrderNumber,
    trg.City=src.City,
    trg.PostCode=src.PostCode

-- 3. ������ �� ������� � trg, �� ���� � src
WHEN NOT MATCHED BY TARGET THEN -- ����������� BY TARGET ����� ���������, �.�. NOT MATCHED = NOT MATCHED BY TARGET
  INSERT(Customername, OrderNumber, Country, City, PostCode)
  VALUES(src.CustomerName,src.OrderNumber,src.Country,src.City,src.PostCode);
	EXEC sp_XML_removedocument @xmlDoc;

	COMMIT TRANSACTION MyTransact
end try
BEGIN CATCH  
    -- Execute error retrieval routine.  
    EXECUTE usp_GetErrorInfo;  
END CATCH; 
go
DECLARE @Request XML;
SET  @Request  = '<Customer>
  <Customer>
    <CustomerName>John</CustomerName>
    <OrderNumber>1</OrderNumber>
    <Country>Ukraine</Country>
    <City>Kremenchug</City>
    <PostCode>39600</PostCode>
  </Customer>
  <Customer>
    <CustomerName>Ivan</CustomerName>
    <OrderNumber>3</OrderNumber>
    <Country>Russia</Country>
    <City>Moscow</City>
    <PostCode>21321</PostCode>
  </Customer>
  <Customer>
    <CustomerName>Ivan</CustomerName>
    <OrderNumber>4</OrderNumber>
    <Country>Russa</Country>
    <City>Moscow</City>
    <PostCode>21321</PostCode>
  </Customer>
  <Customer>
    <CustomerName>Petra</CustomerName>
    <OrderNumber>2</OrderNumber>
    <Country>AZEROT</Country>
    <City>Paris</City>
    <PostCode>45422</PostCode>
  </Customer>
  <Customer>
    <CustomerName>Trall</CustomerName>
    <OrderNumber>5</OrderNumber>
    <Country>Ogtimar</Country>
    <City>Santa Fe</City>
    <PostCode>13212</PostCode>
  </Customer>
  <Customer>
    <CustomerName>Trall</CustomerName>
    <OrderNumber>6</OrderNumber>
    <Country>Turkie</Country>
    <City>Kemer</City>
    <PostCode>21354</PostCode>
  </Customer>
  <Customer>
    <CustomerName>Philip</CustomerName>
    <OrderNumber>7</OrderNumber>
    <Country>Japan</Country>
    <City>Tokyo</City>
    <PostCode>23115</PostCode>
  </Customer>
   <Customer>
    <CustomerName>Ramzec</CustomerName>
    <OrderNumber>8</OrderNumber>
    <Country>Egypy</Country>
    <City>Luxor</City>
    <PostCode>11111</PostCode>
  </Customer>
  <Customer>
    <CustomerName>Alex</CustomerName>
    <OrderNumber>9</OrderNumber>
    <Country>Ukraine</Country>
    <City>Poltava</City>
    <PostCode>21546</PostCode>
  </Customer>
  <Customer>
    <CustomerName>Paul</CustomerName>
    <OrderNumber>10</OrderNumber>
    <Country>Great Britain</Country>
    <City>London</City>
    <PostCode>32112</PostCode>
  </Customer>
  <Customer>
    <CustomerName>Pope</CustomerName>
    <OrderNumber>11</OrderNumber>
    <Country>Rome</Country>
    <City>Milan</City>
    <PostCode>77777</PostCode>
  </Customer>
</Customer>' 
EXEC TransactXML @Request;

