USE Paintings;
GO

CREATE TABLE Paint
(
NameTemp VARCHAR(32),
DescTemp VARCHAR(128)
)
GO

CREATE TRIGGER Painter
ON Paint

INSTEAD OF INSERT
AS
BEGIN
	DECLARE @Id varchar(50);
	DECLARE @Name VARCHAR(32);
	DECLARE @Desc VARCHAR(128);
	DECLARE @table_name varchar(32);	
	
	DECLARE @TestTable UserTableType
BEGIN
SET @Id=@table_name+cast(newid()as varchar(255));
set @table_name='Master'
EXEC UniqID [Master], @Id OUTPUT
	INSERT INTO [Master]([Master id], [Master`s name],[Date of birth])
	VALUES (@Id, @Name,@Desc)
END
END;
GO

INSERT INTO Paint(NameTemp, DescTemp)
VALUES('sadad','ASDASD'),
('ASdasd','ADASD'),
('ASdas','sad');
GO
DROP TABLE Paint
SELECT * FROM [Master]